import React from 'react';
import styled from 'styled-components/macro';

import { COLORS, QUERIES, WEIGHTS } from '../../constants';
import Logo from '../Logo';
import SuperHeader from '../SuperHeader';
import MobileMenu from '../MobileMenu';
import VisuallyHidden from '../VisuallyHidden';
import Icon from '../Icon';

const Header = () => {
  const [showMobileMenu, setShowMobileMenu] = React.useState(false);

  // For our mobile hamburger menu, we'll want to use a button
  // with an onClick handler, something like this:
  //
  // <button onClick={() => setShowMobileMenu(true)}>

  return (
    <header>
      <SuperHeader />
      <MobileHeaderStrip />
      <MainHeader>
        <Side>
          <Logo />
        </Side>
        <Nav>
          <NavLink href="/sale">Sale</NavLink>
          <NavLink href="/new">New&nbsp;Releases</NavLink>
          <NavLink href="/men">Men</NavLink>
          <NavLink href="/women">Women</NavLink>
          <NavLink href="/kids">Kids</NavLink>
          <NavLink href="/collections">Collections</NavLink>
        </Nav>
        <Side />
        <MobileNav>
          <MenuButton>
            <VisuallyHidden>Shopping Cart</VisuallyHidden>
            <Icon id="shopping-bag" strokeWidth={2} />
          </MenuButton>
          <MenuButton>
            <VisuallyHidden>Search</VisuallyHidden>
            <Icon id="search" strokeWidth={2} />
          </MenuButton>
          <MenuButton onClick={() => setShowMobileMenu(true)}>
            <VisuallyHidden>Open Menu</VisuallyHidden>
            <Icon id="menu" strokeWidth={2} />
          </MenuButton>
        </MobileNav>
      </MainHeader>

      <MobileMenu
        isOpen={showMobileMenu}
        onDismiss={() => setShowMobileMenu(false)}
      />
    </header>
  );
};

const MainHeader = styled.div`
  display: flex;
  align-items: baseline;
  padding: 18px 32px;
  height: 72px;
  border-bottom: 1px solid var(--color-gray-300);

  @media (${QUERIES.tabletAndSmaller}) {
    align-items: center;
    padding: 24px 29px 24px 32px;
  }

  @media (${QUERIES.phoneAndSmaller}) {
    padding: 24px 16px;
  }
`;

const Nav = styled.nav`
  display: flex;
  gap: clamp(1rem, 4vw - 2rem, 10rem);
  margin: 0px 48px;

  @media (${QUERIES.tabletAndSmaller}) {
    display: none;
  }
`;

const MobileNav = styled.div`
  display: none;

  @media (${QUERIES.tabletAndSmaller}) {
    display: flex;
    align-items: center;
    gap: 32px;
    color: var(--color-gray-900);
  }

  @media (${QUERIES.phoneAndSmaller}) {
    gap: 20px;
  }
`;

const MenuButton = styled.button`
  background: none;
  border: none;
  padding: 0;
`;

const Side = styled.div`
  flex: 1;
`;

const NavLink = styled.a`
  font-size: 1.125rem;
  text-transform: uppercase;
  text-decoration: none;
  color: var(--color-gray-900);
  font-weight: ${WEIGHTS.medium};

  &:first-of-type {
    color: var(--color-secondary);
  }
`;

const MobileHeaderStrip = styled.div`
  display: none;

  @media (${QUERIES.tabletAndSmaller}) {
    display: revert;
    background-color: var(--color-gray-900);
    height: 4px;
  }
`;

export default Header;
